import React from 'react';
import "./header.css"
import { Link } from 'react-router-dom';

const Header =()=>{
    return (
        <div className="header">
        <div className="hd"><p> <a href="#">Hacker News</a> <a href="#">New</a> | 
            <Link to="/#">Past</Link> | <Link to="/">Home</Link> | <Link to="/welcome">Welcome</Link> | <Link to="/comment">Comments</Link> | <a href="#">Ask</a> |
            <Link to="/#">Show</Link> | <Link to="/#">Jobs</Link> | <Link to="/#">Profile</Link> | <Link to="/#">Submit</Link></p>
            </div>
            <Link to="/login"> Login</Link>
      </div> 
    );
}
export default Header;