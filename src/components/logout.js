import React,{Component} from 'react'
import { Link } from "react-router-dom";

class Logout extends Component {
    constructor(props) {
        super(props);
        localStorage.removeItem("token");
        this.state = {  }
    }
    render() { 
        return ( 
            <>
        <h3>You have been Logged out !!!</h3>
        <Link to="/"></Link>
        </>
         );
    }
}
 
export default Logout;