import React , {useContext} from 'react';
import {newsprovider} from "../news";
import NewsArticle from "./NewsArticle";


function NewsContext(props) {
    const { data } = useContext(newsprovider);
    return (
      <div>
        
        <h1 className="head__text">News App </h1>
        <div className="all__news">
          {data
            ? data.map((news) => (
                <NewsArticle data={news} key={news.url} />
              ))
            : "Loading"}
        </div>
      </div>
    
    );
  }
export default NewsContext;
