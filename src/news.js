import React, {createContext, useEffect, useState } from "react";
import axios from "axios";

export const newsprovider = createContext();

 const Newss = (props) => {
    const [data, setData] = useState();
    //const apiKey = "d3a68d3a93a54948a016a1553bc4d20c";
    
     useEffect(() => {
      axios
        .get('http://newsapi.org/v2/top-headlines?country=in&apiKey=093453f7e02049708c0636f508bdf0aa')
        .then((response) => setData(response.data.articles))
        .catch((error) => console.log(error));
    }, []);
    
  return (<>
  <newsprovider.Provider value={{data}}>{props.children}</newsprovider.Provider>
  </>); 
  
  };
  export default Newss;
