import React from 'react';
import Newss  from "./news";
import NewsContext from "./components/newsContext";
import Header from "./components/header";
import Welcome from "./components/welcome";
import Login from "./components/login";
import Admin from "./components/admin";
import Logout from "./components/logout";
import Comment from "./components/Comment"
import {BrowserRouter , Switch,Route } from 'react-router-dom';
import "./App.css";


function App(){
    return (
       <BrowserRouter>
        <div>
        <Header />
        <Switch>
         <Route path="/" exact component={
             () =>{
                 return (<Newss><NewsContext /></Newss>)
             }
         }/>
         <Route path="/login" component={Login}/>
         <Route path="/welcome" component={Welcome}/>
         <Route path="/comment" component={Comment}/>
         <Route path="/admin" component={Admin} />
         <Route path="/logout" component={Logout}/>
         </Switch>
      </div> 
      </BrowserRouter>
    )
}
export default App;